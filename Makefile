.PHONY: pdf

pdf:	resume.pdf

resume.pdf: resume.cls resume.tex
	pdflatex resume.tex

clean:
	rm -rf resume.pdf resume.log resume.aux
